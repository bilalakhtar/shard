package com.bilal.shard;

import android.content.Context;
import android.database.sqlite.*;

public class ShardDB extends SQLiteOpenHelper {

	private static final int DATABASE_VERSION=2;
	private static final String DATABASE_NAME="ShardDB";
	
	public ShardDB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE subjects (_id INTEGER PRIMARY KEY, title VARCHAR(100) NOT NULL);");
		db.execSQL("CREATE TABLE categories (_id INTEGER PRIMARY KEY, title VARCHAR(100) NOT NULL);");
		db.execSQL("CREATE TABLE shard (_id INTEGER PRIMARY KEY, title VARCHAR(100) NOT NULL, " +
				"subject_id INTEGER NOT NULL," +
				"category_id INTEGER, " +
				"duedate INTEGER, " +
				"FOREIGN KEY (subject_id) REFERENCES subjects(_id), " +
				"FOREIGN KEY (category_id) REFERENCES categories(_id));");
		
		// TODO: Un-hardcode this
		db.execSQL("INSERT INTO subjects VALUES (NULL,'Physics');");
		db.execSQL("INSERT INTO subjects VALUES (NULL,'Chemistry');");
		db.execSQL("INSERT INTO subjects VALUES (NULL,'Math');");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}
