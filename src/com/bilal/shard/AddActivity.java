package com.bilal.shard;

import android.annotation.SuppressLint;
import android.app.*;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import java.util.*;

public class AddActivity extends Activity {

	private SQLiteDatabase db;
	private ProgressDialog progress_dialog;
	private DatePickerFragment newFragment = null;
	
	protected Map<Integer,String> subjects;
	protected Map<Integer,String> categories;
	
	
	private class SaveShard extends AsyncTask<String,Void,Boolean>{

		@Override
		protected Boolean doInBackground(String... args) {
			int subject_id = 0;
			int category_id = 0;
			
			// TODO: Make this failsafe
			Cursor subjects = db.rawQuery("SELECT _id FROM subjects WHERE title='"+args[1]+"';", null);
			Cursor cats = db.rawQuery("SELECT _id FROM categories WHERE title='"+args[2]+"';", null);
			subjects.moveToFirst();
			cats.moveToFirst();
			subject_id = subjects.getInt(0);
			if (args[2] == getString(R.string.dropdown_cats_uncategorized))
				category_id = -1;
			else
				category_id = cats.getInt(0);
			
			String category_string = "NULL";
			db.beginTransaction();
			try {
				if (category_id != -1)
					category_string = String.valueOf(category_id);
				
				db.execSQL("INSERT INTO shard (title,subject_id,category_id,duedate)" +
					" VALUES ('"+args[0]+"', "+subject_id+", "+category_string+", "+args[3]+");");
				
				db.setTransactionSuccessful();
				db.endTransaction();
			
			} catch (Exception e) {
				Log.e("AddActivity", e.getMessage());
				db.endTransaction();
				return false;
			}
			
			return true;
		}
		
		protected void onPostExecute(Boolean result) {
			if (result){
				Toast.makeText(AddActivity.this, getString(R.string.save_successful), Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(AddActivity.this, getString(R.string.save_unsuccessful), Toast.LENGTH_SHORT).show();
			}
			progress_dialog.dismiss();
			AddActivity.this.finish();
		}
		
	}
	
	private class PopulateSpinners extends AsyncTask<Void,Void,Void>{

		protected ArrayAdapter<String> subjects_adapter;
		protected ArrayAdapter<String> categories_adapter;
		
		@Override
		protected Void doInBackground(Void... voids) {
			// Spinner for Subjects
			Cursor c_subjects = db.rawQuery("SELECT * FROM subjects", null);
			subjects = new HashMap<Integer,String>();
			while (c_subjects.moveToNext()) {
				subjects.put(c_subjects.getInt(0), c_subjects.getString(1));
			}
			
			subjects_adapter = new ArrayAdapter<String>(AddActivity.this, android.R.layout.simple_spinner_item, new ArrayList<String>(subjects.values()));
			subjects_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			
			// Spinner for Categories
			Cursor c_cats = db.rawQuery("SELECT * FROM categories", null);
			categories = new HashMap<Integer,String>();
			categories.put(-1, getString(R.string.dropdown_cats_uncategorized)); // Add the Uncategorized view
			while (c_cats.moveToNext()) {
				categories.put(c_cats.getInt(0), c_cats.getString(1));
			}
			
			categories_adapter = new ArrayAdapter<String>(AddActivity.this, android.R.layout.simple_spinner_item, new ArrayList<String>(categories.values()));
			categories_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			
			return null;
		}
		
		protected void onPostExecute(Void result){
			((Spinner)findViewById(R.id.spinner_subject)).setAdapter(subjects_adapter);
			((Spinner)findViewById(R.id.spinner_category)).setAdapter(categories_adapter);
		}
		
	}
	
	public static class DatePickerFragment extends DialogFragment
    	implements DatePickerDialog.OnDateSetListener {
		
		public long duedate = -1;
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 1); // Default will be tomorrow
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			
			duedate = c.getTimeInMillis() / 1000;
			
			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}
		
		public void onDateSet(DatePicker view, int year, int month, int day) {
			// Do something with the date chosen by the user
			Date duedate_date = new Date(year,month,day);
			duedate = (duedate_date.getTime() / 1000);
			Log.d("AddActivity", "Due date: "+duedate);

		}
	}
	
	@SuppressLint("UseSparseArrays")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add);
		
		ShardDB db_helper = new ShardDB(this);
		db = db_helper.getWritableDatabase();
		
		new PopulateSpinners().execute();
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
			
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_activity, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuitem)
	{
		switch (menuitem.getItemId()) {
		case android.R.id.home:
			this.finish();
			break;
			
		case R.id.menu_save:
			progress_dialog = new ProgressDialog(this);
			progress_dialog.setMessage(getString(R.string.progress_dialog));
			
			progress_dialog.show();
			
			saveShard();
			break;
			
		case R.id.menu_add_pic:
			break;
		
		}
		return false;
	}
	
	private void saveShard(){
		
		String title = ((EditText)findViewById(R.id.edittext_title)).getText().toString();
		String subject = (String) ((Spinner)findViewById(R.id.spinner_subject)).getSelectedItem();
		String category = (String) ((Spinner)findViewById(R.id.spinner_category)).getSelectedItem();
		
		String duedate = "NULL";
		
		if (newFragment!=null && ((CheckBox)findViewById(R.id.checkbox_due_date)).isChecked() &&
				newFragment.duedate > 0){
			duedate = String.valueOf(newFragment.duedate);
		} else if (newFragment == null && ((CheckBox)findViewById(R.id.checkbox_due_date)).isChecked()) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			Date tomorrow = calendar.getTime();
			duedate = String.valueOf((tomorrow.getTime()/1000));
		}
		
		Log.d("AddActivity", "Due date: "+duedate);
		// One does not simply save a shard in the UI thread
		new SaveShard().execute(title,subject,category,duedate);
		
	}
	
	public void onCheckboxClicked(View view) {
	    // Is the view now checked?
	    boolean checked = ((CheckBox) view).isChecked();
	    
	    // Check which checkbox was clicked
	    switch(view.getId()) {
	    case R.id.checkbox_due_date:
	    	Button button_due_date = (Button)findViewById(R.id.button_due_date);
	    	
	    	if (checked) {
	    		button_due_date.setEnabled(true);
	    	} else {
	    		button_due_date.setEnabled(false);
	    	}
	    	break;
	    }
	}

	public void onButtonClick(View v) {
		switch (v.getId()){
		case R.id.button_due_date:
		    newFragment = new DatePickerFragment();
		    newFragment.show(getFragmentManager(), "AddActivity");
			break;
		}
	}

}
