package com.bilal.shard;

import android.app.*;
import android.content.Loader;
import android.database.sqlite.SQLiteDatabase;
import android.database.*;
import android.graphics.Color;
import android.view.*;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.os.*;
import android.support.v4.app.ListFragment;

public class ShardList extends ListFragment {
	
	public static final String ARG_SUBJECT_NUMBER = "subject_number";
	
	private SQLiteDatabase db;
	private Integer subject_id;
	private SimpleCursorAdapter list_adapter;
	
	private class RefreshList extends AsyncTask<Void,Void,Void> {

		private Cursor shards;
		@Override
		protected Void doInBackground(Void... arg0) {
			shards = db.rawQuery("SELECT * FROM shard WHERE subject_id = ?", new String[]{String.valueOf(subject_id)});
			shards.moveToFirst(); // TODO: Do sorting here
			return null;
		}
		
		protected void onPostExecute(Void result) {
			list_adapter.swapCursor(shards);
		}
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		
		ShardDB db_helper = new ShardDB(this.getActivity());
		db = db_helper.getReadableDatabase();
		
		subject_id = getArguments().getInt(ARG_SUBJECT_NUMBER);
				
		list_adapter = new SimpleCursorAdapter(this.getActivity(),
				android.R.layout.simple_list_item_1,
				null,
				new String[]{"title"},
				new int[]{android.R.id.text1},
				0);
		
		setListAdapter(list_adapter);
		
		getListView().setBackgroundColor(Color.rgb(220, 220, 220));
		
		new RefreshList().execute();
	}

}
