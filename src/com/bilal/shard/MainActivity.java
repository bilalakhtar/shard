package com.bilal.shard;

import java.util.Locale;

import android.app.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.*;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.*;
import android.widget.*;

public class MainActivity extends FragmentActivity {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SubjectsPagerAdapter mSubjectsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	//private ShardDB db_helper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSubjectsPagerAdapter = new SubjectsPagerAdapter(
				getSupportFragmentManager(),this);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSubjectsPagerAdapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuitem)
	{
		switch (menuitem.getItemId()) {
		case R.id.action_add_shard:
			Intent add_activity = new Intent(this, AddActivity.class);
			startActivity(add_activity);
			
			break;
			
		case R.id.action_settings:
			break;
		}
		return false;
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SubjectsPagerAdapter extends FragmentPagerAdapter {

		private ShardDB db_helper;
		private SQLiteDatabase db;
		private Context context;
		private Cursor subjects;
		
		public SubjectsPagerAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			
			db_helper = new ShardDB(context);
			db = db_helper.getReadableDatabase();
			subjects = db.rawQuery("SELECT * FROM subjects;", null);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			
			Fragment fragment = new ShardList();
			Bundle args = new Bundle();
			subjects.moveToPosition(position);
			int current_subject_id = subjects.getInt(0);
			
			args.putInt(ShardList.ARG_SUBJECT_NUMBER,current_subject_id);
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
						
			return subjects.getCount();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			subjects.moveToPosition(position);
			return subjects.getString(subjects.getColumnIndex("title"));
		}
	}

}
